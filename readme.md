## Drum Kit

The Drum kit was created using JavaScript.

It is able to listen to keystrokes/clicks and play the corresponding sound once the event is detected.

Project for the WebDeveloper Bootcamp from the [AppBrewery](https://www.appbrewery.co/) [Angela Yu](https://twitter.com/yu_angela)

- [Live Demo](https://mnepita.github.io/DrumKit/)

- [Martin Nepita | mnepita.me](https://mnepita.me)
