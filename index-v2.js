/*

//Drum kit

        // create a variable to hold audio sound, play the audio using the 'play' method
        var audio = new Audio('sounds/tom-1.mp3');
        audio.play();

*/

//////////////

/*
Global Functions
*/

// Playing sound with switchStatement
function playSound(testKey) {
    switch (testKey) {
        case "w":
            var tom1 = new Audio("sounds/tom-1.mp3");
            tom1.play();
            break;
        case "a":
            var tom2 = new Audio("sounds/tom-2.mp3");
            tom2.play();
            break;
        case "s":
            var tom3 = new Audio("sounds/tom-3.mp3");
            tom3.play();
            break;
        case "d":
            var tom4 = new Audio("sounds/tom-4.mp3");
            tom4.play();
            break;
        case "j":
            var snare = new Audio("sounds/snare.mp3");
            snare.play();
            break;
        case "k":
            var crash = new Audio("sounds/crash.mp3");
            crash.play();
            break;
        case "l":
            var kick = new Audio("sounds/kick-bass.mp3");
            kick.play();
            break;

        default:
            console.log(testKey);
            break;
    }
};

//Flashing buttons // animation
function buttonAnimation(currentKey) {

    var activeButton = document.querySelector("." + currentKey);

    activeButton.classList.toggle("pressed");

    setTimeout(function () {
        activeButton.classList.toggle("pressed");
    },
        100);
};


//Listeners & triggers

//website listening for keyboard Strokes 
var keyboardPressed = document.addEventListener("keydown", function (event) {
    console.log(event.key);
    playSound(event.key);
    buttonAnimation(event.key);
});


//Adding event Listeners to the buttons
// Set the total number of DrumSets.
var totalDrumButtons = document.querySelectorAll(".drum").length;

//Iterate through all of the buttons to add the addEventListener.
for (var i = 0; i < totalDrumButtons; i++) {

    // Looking through our document querying all of the selector and selecting each individual from the array to add the eventListener.
    document.querySelectorAll(".drum")[i].addEventListener("click", function () {

        var buttonInnerHTML = this.innerHTML;
        playSound(buttonInnerHTML);
        buttonAnimation(buttonInnerHTML);


    });
};




/*
Incorporating jQuery $

*/