/*
//Drum kit

        // create a variable to hold audio sound, play the audio using the 'play' method
        var audio = new Audio('sounds/tom-1.mp3');
        audio.play();

*/


// Set the total number of DrumSets.
var totalDrumButtons = document.querySelectorAll(".drum").length;

//Iterate through all of the buttons to add the addEventListener.
for (var i = 0; i < totalDrumButtons; i++) {

    // Looking through our document querying all of the selector and selecting each individual from the array to add the eventListener.
    document.querySelectorAll(".drum")[i].addEventListener("click", function () {
        var sounds = {
            'w': 'sounds/tom-1.mp3',
            'a': 'sounds/tom-2.mp3',
            's': 'sounds/tom-3.mp3',
            'd': 'sounds/tom-4.mp3',
            'j': 'sounds/snare.mp3',
            'k': 'sounds/crash.mp3',
            'l': 'sounds/kick-bass.mp3'
        };
        var audio = new Audio(sounds[this.innerHTML]);
        audio.play();

        this.style.color = "orange";
    });

}



